
#define CPPBUILD_FOR_TEST
#include "../cppbuild.hpp"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

TEST_CASE("creating a folder without slashes in the name, it creates one folder", "[folder]") {
    cppbuild::Folder folder("test");

    REQUIRE(folder.folders().size() == 0);

    auto folders = folder.allFolders();
    REQUIRE(folders.size() == 1);
    REQUIRE(folders[0] == "test");

    SECTION("creating a folder and add two sub folders") {
        cppbuild::Folder sub1("sub1");
        cppbuild::Folder sub2("sub2");

        folder.folder(sub1);
        folder.folder(sub2);

        auto folders = folder.allFolders();
        REQUIRE(folders.size() == 3);
        REQUIRE(folders[0] == "test");
        REQUIRE(folders[1] == "test\\sub1");
        REQUIRE(folders[2] == "test\\sub2");
    }
}

TEST_CASE("creating a folder with a slash in the name, it creates two folders", "[folder]") {
    cppbuild::Folder folder("test\\subfolder");

    REQUIRE(folder.folders().size() == 1);

    auto folders = folder.allFolders();
    REQUIRE(folders.size() == 2);
    REQUIRE(folders[0] == "test");
    REQUIRE(folders[1] == "test\\subfolder");
}

TEST_CASE("creating a folder with alternative slashes in the name, it creates two folders", "[folder]") {
    cppbuild::Folder folder("test/subfolder");

    REQUIRE(folder.folders().size() == 1);

    auto folders = folder.allFolders();
    REQUIRE(folders.size() == 2);
    REQUIRE(folders[0] == "test");
    REQUIRE(folders[1] == "test\\subfolder");
}

TEST_CASE("creating a folder with both slashes and alternative slashes in the name, it creates three folders", "[folder]") {
    cppbuild::Folder folder("test/subfolder\\subsub");

    REQUIRE(folder.folders().size() == 1);

    auto folders = folder.allFolders();
    REQUIRE(folders.size() == 3);
    REQUIRE(folders[0] == "test");
    REQUIRE(folders[1] == "test\\subfolder");
    REQUIRE(folders[2] == "test\\subfolder\\subsub");
}

TEST_CASE("Folder '+=' operator tests", "[folder]") {
    cppbuild::Folder folder1("test\\a");
    cppbuild::Folder folder2("test\\b");

    REQUIRE(folder1.folders().size() == 1);
    folder1 += folder2;
    REQUIRE(folder1.folders().size() == 2);

    auto folders = folder1.allFolders();
    REQUIRE(folders.size() == 3);
    REQUIRE(folders[0] == "test");
    REQUIRE(folders[1] == "test\\a");
    REQUIRE(folders[2] == "test\\b");
}