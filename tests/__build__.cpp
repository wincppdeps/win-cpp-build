
#include <cppbuild.hpp>

int main(int argc, char* argv[])
{
    cppbuild::init(argc, argv);
    
    cppbuild::Target testFolder("test-folder");
    
    testFolder.files({
        "test-folder.cpp" 
    });

    return 0;
}
