#include <iostream>
#include <glm/glm.hpp>
#include <GLFW/glfw3.h>

int main(int argc, char* argv[])
{
    if (glfwInit() == GLFW_FALSE)
        return -1;

    auto window = glfwCreateWindow(800, 600, "cppbuild test", NULL, NULL);
    if (window == nullptr)
    {
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);

    glClearColor(0.6f, 0.8f, 1.0f, 1.0f);
    
    while (glfwWindowShouldClose(window) == 0)
    {
        glfwPollEvents();

        glClear(GL_COLOR_BUFFER_BIT);

        glfwSwapBuffers(window);

    }

    glfwTerminate();

    return 0;
}
