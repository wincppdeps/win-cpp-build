
#include <cppbuild.hpp>

int main(int argc, char* argv[])
{
    cppbuild::init(argc, argv);

    cppbuild::Target target("glfw3-project");

    target.files({
        "program.cpp" 
    });

    target.libraries({
        "glfw3",
        "opengl32",
        "gdi32",
    });

    target.includeDirs({
        "%WIN_CPP_PREFIX_PATH%\\include"
    });

    target.libraryDirs({
        "%WIN_CPP_PREFIX_PATH%\\lib"
    });

    return 0;
}
