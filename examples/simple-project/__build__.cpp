
#include <cppbuild.hpp>

int main(int argc, char* argv[])
{
    cppbuild::init(argc, argv);
    
    cppbuild::Target target("simple-project");
    
    target.files({
        "program.cpp" 
    });

    return 0;
}
