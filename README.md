# win-cpp-build #
When you have a working mingw compiler on your Windows machine, you can use this script to build your simple c++ project. In your project build "script" you write regular c++ that will describe your project per targets through files, include directories, library directories and libraries. This makes your whole c++ project written in c++!

## Example ##
Below an example **__build__.cpp** file for a c++ project with one file and a dependancy to **glfw3**, **opengl32** and **gdi32**.

```
#!c++
#include <cppbuild.hpp>

void project()
{
    cppbuild::Target target("project");
    
    target.files({
        "program.cpp" 
    });

    target.libraries({
        "glfw3",
        "opengl32",
        "gdi32",
    });

    target.includeDirs({
        "%WIN_CPP_PREFIX_PATH%\\include"
    });

    target.libraryDirs({
        "%WIN_CPP_PREFIX_PATH%\\lib"
    });
}

```

## Environment setup ###

For the build script te work, **cppbuild.cmd** from this repository should be available through the enviroment PATH. The **cppbuild.hpp** should be next to it. Furthermore you need a working mingw compiler on the environment PATH. You can test both by running: 

```
#!
g++ --version
```
and
```
#!
cppbuild --version
```

## Usage ###

I assume you have a command prompt open at your project directory containing **__build__.cpp** and the environment setup. To build your project you execute the following command:

```
#!
cppbuild
```
or
```
#!
cppbuild build
```

To run your project, you execute the following command:

```
#!
cppbuild run
```

To cleanup the files that were build your, you execute the following command:

```
#!
cppbuild clean
```
That;'s it!

## TODO ##
Things todo are some of the following:

* compiler and linker flag support
* Debug/Release support
* Add install command

###Fixed todos:###

* Optimize building object files only when they are older that the source file